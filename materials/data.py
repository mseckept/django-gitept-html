
ept = "Ecole Polytechnique de Thies"

liste = ["Mamadou", 100000, "Doudou", 9.899999]

config = {
    "is_connected": True,
    "is_prime": True,
    "prenom": "Amadou",
    "email": "asanoko@ept.sn",
    "age": 40
}

table = [
    {"prenom": "Thiané", "nom": "DIA", "id": 83},
    {"prenom": 'Mouhamed', "nom": 'DIAGNE', "id": 70},
    {"prenom": "Abdoulaye", "nom": "DIALLO", "id": 84},
    {"prenom": "Cheikh Ibra", "nom":  "DIENG", "id": 90},
    {"prenom": "Mouhamed", "nom": "DIENG", "id": 74},
    {"prenom": "Assane", "nom": "DIOUF", "id": 89},
    {"prenom": "Ndaraw", "nom": "FALL", "id": 37},
    {"prenom": "Pape Ibrahima Konaté ", "nom": "GNINGUE", "id": 10},
    {"prenom": "Mouhamed", "nom": "GUEYE", "id": 54},
    {"prenom": "Isabelle Olive", "nom": "KANTOUSSAN", "id": 82},
    {"prenom": "Mouhamadou Mansour", "nom": "KHOLLE", "id": 12},
    {"prenom": "Mouhamadou Lamine", "nom": "MAR", "id": 111},
    {"prenom": "Aïssatou Kany Diogop", "nom": "MBODJE", "id": 222},
    {"prenom": "Mariama", "nom": "MBODJI", "id": 61},
    {"prenom": "El hadji", "nom": "MBOUP", "id": 98},
    {"prenom": "Ndèye Marième", "nom": "NDIAYE", "id": 65},
    {"prenom": 'Pape Mor', "nom": "NDIAYE", "id": 88},
    {"prenom": "Hamady", "nom": "SAKANOKO", "id": 99},
    {"prenom": 'Gora', "nom": "SECK", "id": 73},
    {"prenom": "Thierno Saydou", "nom": "TALLA", "id": 65},
    {"prenom": "Serigne Saliou", "nom": "THIAM", "id": 66},
    {"prenom": "Mouhamad Samba", "nom": "TRAORE", "id": 55},
]
